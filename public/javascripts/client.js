var socket = io();

// URL RegExp from https://mathiasbynens.be/demo/url-regex
var urlRegExp = /^(?:(?:https?|ftp):\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/\S*)?$/i;

document.addEventListener('DOMContentLoaded', function() {
    // DOM ready, run it!


}, false);

// Pull the trigger…
function fire(url) {

	if (url.match(urlRegExp) != null) {
		// …if we have a proper URL

		socket.emit('fire', {
			'url': url,
			'hash': makeid()
		});

	}

}

function notBusy() {
	socket.emit('notBusy');
}

// *click*
socket.on('hit', function(obj){

});

function makeid() {
	/* via http://stackoverflow.com/questions/1349404/generate-a-string-of-5-random-characters-in-javascript */

	var text = "";
	var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

	for (var i = 0; i < 5; i++) {
		text += possible.charAt(Math.floor(Math.random() * possible.length));
	}

	return text;
}