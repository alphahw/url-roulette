var express = require('express');
var app = express();
var http = require('http').Server(app);
var fs = require('fs');
var io = require('socket.io')(http);

app.set('port', (process.env.PORT || 3000));
app.use(express.static('public'));

app.get('/', function(request, response){
	response.sendFile(__dirname + '/public/index.html');
});

var connectedUsers = 0,
	clients = [],
	busyClients = [];

// URL RegExp from https://mathiasbynens.be/demo/url-regex
var urlRegExp = /^(?:(?:https?|ftp):\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/\S*)?$/i;

// Simple blacklisting
var blacklist = [];

fs.watch('./blacklist', function(c, p) { update_blacklist(); });

function update_blacklist() {
  sys.log("Updating blacklist.");
  blacklist = fs.readFileSync('./blacklist').split('\n')
              .filter(function(rx) { return rx.length })
              .map(function(rx) { return RegExp(rx) });
}

io.on('connection', function(socket){

	var client_ip = socket.request.connection.remoteAddress;

	console.log( client_ip + ' (' + socket.id + ') connected.');

	clients.push(socket);
	connectedUsers++;

	io.emit('users', connectedUsers);
	
	socket.on('fire', function(obj){

		obj = JSON.parse(obj);
		
		if (obj.url.match(urlRegExp) != null) {

			console.log( client_ip + ' (' + socket.id + ') fired "' + obj.url + '" [#' + obj.hash + ' FIRE]');

			if (clients.length > 1) {
				// The supplied data is really a URL, and we have someone else to send it to

				if (blacklist.indexOf(obj.url) != -1) {
					
					// Blacklisted!
					console.log( client_ip + ' (' + socket.id + ')\'s URL "' + obj.url + '" is blacklisted. Transmission terminated. [#' + obj.hash + ' BLACKLISTED]');

					socket.emit('blacklisted', obj);

				} else {

					var randomClient = clients[Math.floor(Math.random() * clients.length)];

					// Is the random client already busy or ourself? Try again
					while (busyClients.indexOf(randomClient) != -1 || randomClient === socket) {
						randomClient = clients[Math.floor(Math.random() * clients.length)];
					}
					
					randomClient.emit('hit', obj);
					randomClient.currentTurnHash = obj.hash;

					var randomClient_ip = randomClient.request.connection.remoteAddress;

					console.log( randomClient_ip + ' (' + randomClient.id + ') was hit by "' + obj.url + '" and is now BUSY. [#' + obj.hash + ' HIT]');

				}

			}

		}

	});

	socket.on('blacklist', function(obj){

		if (blacklist.indexOf(obj.url) == -1) {

			fs.appendFile('./blacklist', obj.url + '\n', function(err){

				if (err) {

					// Failure
					console.log('Blacklisting of "' + obj.url + '" failed. [#' + obj.hash + ' BLACKLIST_FAILED]');

					socket.emit('blacklistFailed', obj);

				} else {

					// Success
					console.log('Blacklisting of "' + obj.url + '" successful. [#' + obj.hash + ' BLACKLIST_SUCCESSFUL]');

					socket.emit('blacklistSuccessful', obj);

				}

			});

		};

	});

	socket.on('notBusy', function(){

		var index = busyClients.indexOf(socket);
        
        if (index != -1 && socket.currentTurnHash) {
            busyClients.splice(index, 1);
			console.log( client_ip + ' (' + socket.id + ') is NO LONGER BUSY. [#' + socket.currentTurnHash + ' NOT_BUSY]');
        }

	});

	socket.on('disconnect', function(){
		
		var index = clients.indexOf(socket);
        
        if (index != -1) {
            clients.splice(index, 1);
            connectedUsers--;
            console.log( client_ip + ' (' + socket.id + ') disconnected.');
        }

        io.emit('users', connectedUsers);

	});

});

http.listen(app.get('port'), function(){
	console.log('listening on port ' + app.get('port'));
});